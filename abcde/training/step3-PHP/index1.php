<?php
/*
 * -Để khai báo biến trong PHP thì nó cũng có một số nguyên tắc sau:
 * Biến phải được bắt đầu bằng ký tự $.
 * Tên biến phải được bắt đầu bằng các chữ cái hoặc dấu _ không được bắt đầu bằng số, và các ký tự khác ngoài
 * php là 1 ngôn ngữ phân biệt chữ hoa chữ thường
 * dùng dấu = để gán giá trị
 *biến có thể thay đổi được còn hằng thì khônng
 * define('tenHang', 'giaTri');
 * define là cú pháp mặc định để tạo hàm trong php
 * kiểu int không dùng dấu ngoặc nháy
 * Kiểu boolean trong PHP là một kiểu dữ liệu mà giá trị của nó chỉ tồn tại 2 giá trị TRUE,FALSE (có thể viết hoa, thường cũng được).
 * kiểu string phải được đặt trog các dấu nháy
 * cách khai báo mảng $array = array(); hoặc $array = [];
 * hàm print_r() để in ra tất cả các phần tử và vị trí của nó trong mảng
 *dùng hàm var_dump để xem dữ liệu đang ở kiểu nào

 */

$tientien='rùa hạ thanh thùy thanh oai hà nội';
echo $tientien;
echo '<br>';

define('nguyen','hoangnguyencamera');
echo nguyen;
echo '<br>';

//in ra tên từng người theo thứ tự
$ten = ['tiên','huyền','ngọc','linh'];
echo $ten[0];
echo '<br>';

//in ra tất cả
$ten = ['tiên','huyền','ngọc','linh'];
print_r($ten);
echo '<br>';

$arr = array('Tôi','Đi');
$arr[] = 'Học';
print_r($arr);

$a = 5 ;
echo '<br>';
$b = 'tôi đi code';
echo $a,$b;
echo '<br>';


$tuoi = 19;
if ($tuoi<18){
    echo 'bạn chưa được 18 tuổi';
}else{
    echo 'bạn đã đủ tuổi lấy vk';
}

$i=0;
while ($i<=10){
    echo $i . "<br>" .'số nè';
    $i++;
}










